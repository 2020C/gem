<div id="footer" class="row">
    
    <div class="col-md-10 col-md-offset-1">
        
    <div class="row">
          
             <div class="col-md-6 footer-gem">
        
                <p>&copy; <?php echo of_get_option( 'gem_co_name', 'no entry' ); ?> | <?php echo of_get_option( 'gem_co_phone', 'enter phone number' ); ?></p>
                 
            </div>
            <div class="col-md-6">
            
                <p class="pull-right"><a href="http://2020creative.com" id="2020 Creative" title="website design">2020 Creative - v1.12</a></p>
                 
            </div>
			
    </div>
        
    </div>  
        
    
</div> <!-- end footer -->       
        
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>